package org.example

class ExtProperties {
  def properties = [:] as Map<String, Object>

  void ext(Closure closure) {
    closure.delegate = properties
    closure.resolveStrategy = Closure.DELEGATE_ONLY
    closure.call()
  }
}

def extProps = new ExtProperties()

extProps.ext {
  var1 = 'zaa'
  var2 = 12
  var3 = true
}

// Access the properties
println """
var1: ${extProps.properties.var1}
var2: ${extProps.properties.var2}
var3: ${extProps.properties.var3}
"""

println "$extProps.properties"

// A simple hello world example using the properties
def sayHello(extProps) {
  println "Hello, ${extProps.properties.var1}!"
}

// Call the sayHello method and pass the extProps instance
sayHello(extProps)


def map = [a = 1, b = 2]
def map2 = [a : 1, b : 2]
print map
print map2


/*
In Groovy, closures can be assigned a delegate, which is an object to which method calls and
property references will be resolved if they are not found in the closure itself. This is particularly
useful for building DSLs (Domain-Specific Languages) and dynamic code execution. The delegate
allows you to customize how a closure behaves when it references properties and methods.

The delegate is the object that the closure will use to resolve any property or method references that it makes.
*/
