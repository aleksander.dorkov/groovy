package org.example


def myClosure0 = {
  println('myClosure0 Hello world')
}

def str = 'str value'
// can ref variables outside of it, methods can not
def myClosure1 = { name ->
  println("myClosure1 Hello world $name $str")
}

def myClosure2 = {
  a, b, c -> return a + b + c
}

// closures as variables of method
static def myMethod(Closure close) {
  print('from method: ')
  close.call("aaaa")
}

myClosure0.call()
myClosure1.call("bb")
println(myClosure2.call(1, 2, 3))
int result = myClosure2(1, 2, 3) as int;
println(result)
myMethod(myClosure1)
myMethod({ name -> println("Inline colsure $name") })


def myList = [1, 2, 3, 4, 5];
myList.each {
  println(it)
}


