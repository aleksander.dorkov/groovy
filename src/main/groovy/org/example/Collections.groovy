package org.example

static void main(String[] args) {
  // List
  def list = ['Apple', 'Banana', 'Cherry']
  println "List:"
  list.each { item -> println item }

// Set
  def set = ['Apple', 'Banana', 'Cherry'] as Set
  println "\nSet:"
  set.each { item -> println item }

  // Map
  def map = [Apple: 1, Banana: 2, Cherry: 3] as Map<String, Integer>
  println "\nMap:"
  map.each { key, value -> println "$key: $value" }

  // Array
  String[] array = ['Apple', 'Banana', 'Cherry']
  println "\nArray:"
  array.each { item -> println item }
}
