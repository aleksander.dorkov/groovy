package org.example

static void main(String[] args) {
  def obj = new MyClass();
  println(obj.greet())
  println(obj.greet2('aa'))
  println(obj.greet3('aa', 12))
}

class MyClass {
  static def greet(String name = 'World') {
    return "Hello, $name"
  }

  String greet2(String name = 'World') {
    return "Hello, $name"
  }

  def greet3(String name, age) {
    return "Hello, $name $age"
  }
}

