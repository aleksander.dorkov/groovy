package org.example

static void main(String[] args) {
  println "Hello world!"
  def b = 1 as byte
  def s = 2 as short
  def i = 3
  def l = 4L
  def f = 5.0f
  def d = 6.0
  def c = 'A'
  def bool = true

  println "byte: $b\nshort: $s\nint: $i\nlong: $l\nfloat: $f\ndouble: $d\nchar: $c\nboolean: $bool"


  println("\nOther way:")
  int a = 12
  String greeting = "Hello, Groovy"
  double price = 19.99
  boolean flag = true

  println "int a: $a\ngreeting: $greeting\nprice: $price\nflag: $flag"

  //  Single vs Quote quote strings
  println 'This is a simple string. ${greeting}' // This is a simple string. ${greeting}
  println "This is a simple string. ${greeting}" // This is a simple string. Hello, Groovy
}
